# IdentifyExportToMySql Conversion tool

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

## Installation

1. Create two folders: one for the program and another for data working folder.
2. Download this program into the new program folder.
3. Edit this php script to configure $csvPath with the path to your data working folder.

## Execution

1. Downloaded the desired (latest. CSV pkg (zip. from Ringgold Sharefile into the data working folder.
2. Unzip the pkg file into a subfolder and record the path of the subfolder
3. Modify the $csvPath variable to indicate the path to locate the csv files
4. Enter
		php IdentifyConvertCsvMysql57.php
	or	./run.bat
5. Your output file will be located in the program folder.

## SQL Template

1. the Class in this script, looks for and converts the following placeholders, in the template, during output
{YYYYMMDD} is converted to the run date. example "identify_{YYYYMMDD}" becomes "identify_20200207"
{DATABASE} is converted to the current database name when the class is instantiated. Look for $databaseName below. 
Currently it's set to identify_YYYYMMDD. 

## Authors

* **William Haase** - *Initial work* - [bill.haase@ringgold.com](mailto:bill.haase@ringgold.com)
* [Ringgold Inc.](https://www.ringgold.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details