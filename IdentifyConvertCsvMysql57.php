<?php
/**
 * IdentifyConvertCsvMySql57.php
 * @author = bill.haase@ringgold.com
 * @author = ehaase@copyright.com
 * @version = 20230314
 */


/**
 * README
 * Installation:
 * 1) create two folders: one for the program and another for data working folder.
 * 2) download this program and sql folder into a new program folder.
 * 3) edit this php script to configure $csvPath with the path to your data working folder.
 * 
 * Execution:
 * 1) downloaded the desired (latest) CSV pkg (zip) from Ringgold Sharefile into the data working folder.
 * 2) unzip the pkg file into a subfolder and record the path of the subfolder
 * 3) modify the $csvPath variable to indicate the path to locate the csv files
 * 4) enter
 * 		php IdentifyConvertCsvMysql57.php
 * 5) your output file will be located in the program folder.
 * 
 * SQL Template:
 * 1) the Class in this script, looks for and converts the following placeholders in the template during output
 * {YYYYMMDD} is converted to the run date. example "identify_{YYYYMMDD}" becomes "identify_20200207"
 * {DATABASE} is converted to the current database name when the class is instantiated. Look for $databaseName below. Currently it's set to identify_YYYYMMDD. 
 * 
 */


// to allow enough time to execute. Most php.ini configs limit to 30 seconds. We need more than that.
set_time_limit(0);

// shut off buffering for progress reporting
// ob_implicit_flush();
// ob_end_clean();


/**
 * user config
 * set $csvPath to location of your downloaded, unpacked csv files
 */
$csvPath = "/workspaces/Debugging/ExportFiles/24Ringgold_Identify_csv/";



/**
 * The following should not require changes
 */
$runDate = date('Ymd');
$databaseName = sprintf('identify_%s', $runDate);
$outFilename = sprintf('identify_%s.sql', $runDate);
$csvPrefix = "Ringgold_Identify_csv_";
$csvTables = [
	'Ontology' => 'ontology',
	'OrgAltNames' => 'org_alt_names',
	'Organizations' => 'organizations',
	'OrganizationsDeleted' => 'organizations_deleted',
	'OrgClassifications' => 'org_classifications',
	'OrgExternalClassifications' => 'org_external_classifications',
	'OrgExternalIdentifiers' => 'org_external_identifiers',
	'OrgNotes' => 'org_notes',
	'OrgRelationships' => 'org_relationships',
	'OrgSizes' => 'org_sizes',
	'OrgUrls' => 'org_urls',
	'Places' => 'places'
];

$c = new CsvSqlConverter($outFilename, $databaseName, $runDate);
try {


	/**
	 * enable this section to read the embedded sql template in the class to the output
	 */
	$c->addSqlTemplate();


	/**
	 * enable this section to read a single sql template for output
	 */
	// $c->addFile('sql/identify_mysql_5_7.sql');


	/**
	 * enable this to combine several sql/t_*.sql files into one sql output
	 */
	// $c->addFile('sql/create_database.sql');
	// $c->addSqlFiles('sql');


	/**
	 * process all the csv tables into one large sql script
	 */
	foreach ($csvTables as $csv => $table) {
		// echo $csvPath . $csvPrefix . $csv . '.csv', ' to ', $table, "\n";
		// echo filesize($csvPath . $csvPrefix . $csv . '.csv'), "\n";
		$c->convertCsv($csvPath . $csvPrefix . $csv . '.csv', $table);
	}
} finally {
	$c->close();
}

echo "Finished\n";
exit;


/**
 * this class provides helper functions to convert the csv data into mysqldump compatible scripts
 */
class CsvSqlConverter
{
	private $outFilename;
	private $databaseName;
	private $search = [];
	private $replace = [];
	private $fpOut;

	private $headers;
	private $lastModifiedHeaderIndex;

	public function __construct($outFilename, $databaseName, $runDate)
	{
		echo "runDate: $runDate\n";
		echo "database: $databaseName\n";
		echo "output: $outFilename\n";
		$this->outFilename = $outFilename;
		$this->databaseName = $databaseName;
		if (($this->fpOut = fopen($this->outFilename, 'c+')) === false)
			throw new \RuntimeException("Unable to open file $this->outFilename");

		$block = true;
		$flag = LOCK_EX;
		if (!$block)
			$flag |= LOCK_NB;

		if (!flock($this->fpOut, $flag))
			throw new \RuntimeException("Unable to acquire file lock");

		ftruncate($this->fpOut, 0);

		$this->search = ['{YYYYMMDD}', '{DATABASE}'];
		$this->replace = [$runDate, $databaseName];
	}

	public function addSqlTemplate()
	{
		$output = $this->getSqlTemplate();
		$output = str_replace($this->search, $this->replace, $output);
		$this->output($output . "\n\n");
	}

	public function addFile($inFilename)
	{
		echo "addFile: file: $inFilename\n";
		$inFp = fopen($inFilename, 'r');
		$data = fread($inFp, filesize($inFilename));
		$output = str_replace($this->search, $this->replace, $data);
		$this->output($output . "\n\n\n\n");
		fclose($inFp);
	}

	public function addSqlFiles($dir)
	{
		$files = scandir($dir);
		foreach ($files as $inFilename) {
			if (substr($inFilename, 0, 1) == 't') {
				$inFilename = $dir . '/' . $inFilename;
				$this->addFile($inFilename);
			}
		}
	}

	private $buffer = "";
	private $buflen = 0;
	private function buffer($fmt, ...$params)
	{
		$str = sprintf($fmt, ...$params);
		$this->buffer .= $str;
		$this->buflen += strlen($str);
		if ($this->buflen < 8192)
			return;
		$this->flush();
	}

	private function flush()
	{
		fwrite($this->fpOut, $this->buffer);
		fflush($this->fpOut);
		$this->buffer = "";
		$this->buflen = 0;
	}

	private function output($output)
	{
		// fwrite($this->fpOut,$output);
		$this->buffer($output);
	}

	public function close()
	{
		$this->flush();
		// fflush($this->fpOut);
		flock($this->fpOut, LOCK_UN);
		return fclose($this->fpOut);
	}

	public function readCsvGen($inFilename)
	{
		$inFp = fopen($inFilename, 'r');
		printf("$inFilename open: %d byte(s)\n", filesize($inFilename));
		$headers = trim(fgets($inFp));
		$tmp = str_replace('"', '', $headers);
		$headerArray = explode(',', $tmp);
		$headers = str_replace('"', '`', $headers);
		$this->headers = $headers;
		$lastModifiedHeaderIndex = array_search("last_modified", $headerArray);
		$read = 0;
		while (!feof($inFp)) {
			$line = fgetcsv($inFp);
			$read++;
			if (empty($line))
				continue;
			if ($lastModifiedHeaderIndex != false) {
				$line[$lastModifiedHeaderIndex] = strftime("%Y-%m-%d %H:%M:%S", strtotime($line[$lastModifiedHeaderIndex]));
			}
			yield $line;
		}
		fprintf(STDERR, "Closing %s (read: %d)\n", $inFilename, $read);
		fclose($inFp);
	}

	public function chunkGen($gen, $size = 16000)
	{
		$once = new NoRewindIterator($gen);
		while ($once->valid())
			yield new LimitIterator($once, 0, $size);
	}

	public function convertCsv($inFilename, $table)
	{
		$this->headers = null;
		$this->lastModifiedHeaderIndex = null;

		$lines = $this->readCsvGen($inFilename);
		$chunks = $this->chunkGen($lines, 16000);

		$total = 0;
		foreach ($chunks as $chunk) {
			$this->buffer("insert into %s.%s (%s) values ", $this->databaseName, $table, $this->headers);
			$lineCount = 0;
			foreach ($chunk as $line) {
				$total++;
				if ($lineCount++ > 0) $this->buffer(",\n");

				$arr = array_map(function ($e) {
					return sprintf('"%s"', addslashes($e));
				}, $line);

				$this->buffer("(%s)", implode(',', $arr));
			}
			$this->buffer(";\n");
		}
		$this->buffer("\n\n");

		echo "wrote $total rows\n";
	}

	private function getSqlTemplate()
	{
		return <<<EOD
DROP DATABASE if exists `{DATABASE}`;

CREATE DATABASE `{DATABASE}` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

# "internal_id","taxonomy_name","term_id","term_name","term_description","last_modified"
CREATE TABLE {DATABASE}.ontology (
	internal_id INT (11) NOT NULL,
	taxonomy_name VARCHAR (255),
	term_id INT (11),
	term_name VARCHAR (255),
	term_description text,
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (taxonomy_name, term_id),
	INDEX (taxonomy_name, term_name),
	INDEX (term_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","name_type_term_id","name_type_year","name","language","place_id","post_code","last_modified"
CREATE TABLE {DATABASE}.org_alt_names (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	name_type_term_id INT (11),
	name_type_year varchar (10),
	`name` varchar (255),
	`language` varchar (5),
	place_id int (11),
	post_code varchar (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "ringgold_id","name","place_id","post_code","is_consortium","last_modified"
CREATE TABLE {DATABASE}.organizations (
	ringgold_id INT (11) NOT NULL,
	`name` VARCHAR (255),
	place_id INT (11),
	post_code VARCHAR (255),
	is_consortium char (1),
	last_modified TIMESTAMP,
	PRIMARY KEY (ringgold_id)
) ENGINE = INNODB;

# "internal_id","old_ringgold_id","new_ringgold_id","details","last_modified"
CREATE TABLE {DATABASE}.organizations_deleted (
	internal_id INT (11) NOT NULL,
	old_ringgold_id INT (11),
	new_ringgold_id int (11),
	details varchar (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	index (old_ringgold_id),
	index (new_ringgold_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","classification_type_term_id","classification_value_term_id","last_modified"
CREATE TABLE {DATABASE}.org_classifications (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	classification_type_term_id INT (11),
	classification_value_term_id int (11),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "iternal_id","ringgold_id","classification_type_term_id","classification_value","last_modified"
CREATE TABLE {DATABASE}.org_external_classifications (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	classification_type_term_id INT (11),
	`classification_value` VARCHAR (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","identifier_type_term_id","identifier_value","last_modified"
CREATE TABLE {DATABASE}.org_external_identifiers (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	identifier_type_term_id INT (11),
	`identifier_value` VARCHAR (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","note","last_modified"
CREATE TABLE {DATABASE}.org_notes (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	`note` text,
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "internal_id","subject_ringgold_id","object_ringgold_id","relationship_type_term_id","last_modified"
CREATE TABLE {DATABASE}.org_relationships (
	internal_id INT (11) NOT NULL,
	subject_ringgold_id INT (11) NOT NULL,
	object_ringgold_id INT (11),
	relationship_type_term_id int (11),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (subject_ringgold_id),
	index (object_ringgold_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","size_type_term_id","value","last_modified"
CREATE TABLE {DATABASE}.org_sizes (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	size_type_term_id INT (11),
	`value` VARCHAR (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;

# "internal_id","ringgold_id","url","url_type_term_id","last_modified"
CREATE TABLE {DATABASE}.org_urls (
	internal_id INT (11) NOT NULL,
	ringgold_id INT (11) NOT NULL,
	`url` VARCHAR (255),
	url_type_term_id INT (11),
	last_modified TIMESTAMP,
	PRIMARY KEY (internal_id),
	INDEX (ringgold_id)
) ENGINE = INNODB;


# "place_id","language","country_code","name","name_short",
# "administrative_area_level_1","administrative_area_level_2","administrative_area_level_3","administrative_area_level_4","administrative_area_level_5",
# "administrative_area_level_1_short","administrative_area_level_2_short","administrative_area_level_3_short","administrative_area_level_4_short","administrative_area_level_5_short",
# "longitude","latitude","formatted_name","country_format_pattern","last_modified".
CREATE TABLE {DATABASE}.places (
	place_id INT (11) NOT NULL,
	`language` varchar (5),
	country_code char (2),
	`name` varchar (255),
	name_short varchar (255),
	administrative_area_level_1 varchar (255),
	administrative_area_level_2 varchar (255),
	administrative_area_level_3 varchar (255),
	administrative_area_level_4 varchar (255),
	administrative_area_level_5 varchar (255),
	administrative_area_level_1_short varchar (255),
	administrative_area_level_2_short varchar (255),
	administrative_area_level_3_short varchar (255),
	administrative_area_level_4_short varchar (255),
	administrative_area_level_5_short varchar (255),
	longitude float,
	latitude float,
	formatted_name varchar (255),
	country_format_pattern varchar (255),
	last_modified TIMESTAMP,
	PRIMARY KEY (place_id,`language`)
) ENGINE = INNODB;		
EOD;
	}
}
